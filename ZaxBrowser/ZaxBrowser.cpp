/*
 * ZaxBrowser - A web browser
 * 
 * Created by Izaak Webster
 * 
 */

#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <CommCtrl.h>

#include "UI.h"
#include "ZaxBrowser.h"

HINSTANCE hInstGlobal;
HMENU hMenu;
HWND hToolbar;

int WINAPI WinMain(
    _In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPSTR     lpCmdLine,
    _In_ int       nCmdShow
)
{
    WNDCLASSEX wcex = UI::prepareWindow(hInstance);

    if (!RegisterClassEx(&wcex))
    {
        MessageBox(NULL,
            _T("Call to RegisterClassEx failed!"),
            szTitle,
            NULL);

        return 1;
    }

    // Store instance handle in our global variable
    hInstGlobal = hInstance;

    HWND hWnd = UI::createWindow(hInstance);

    if (!hWnd)
    {
        MessageBox(NULL,
            _T("Call to CreateWindow failed!"),
            szTitle,
            NULL);

        return 1;
    }

    // The parameters to ShowWindow explained:
    // hWnd: the value returned from CreateWindow
    ShowWindow(hWnd, SW_MAXIMIZE);
    UpdateWindow(hWnd);

    // Main message loop:
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int)msg.wParam;
}

//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    RECT rcClient;
    RECT rcTool;
    int iToolHeight;
    int iEditHeight;

    switch (message)
    {
    case WM_CREATE:
        UI::createMenu(hWnd);
        break;
    case WM_PAINT:
        return UI::paintWindow(hWnd, hInstGlobal, hMenu, hToolbar);
        break;
    case WM_DESTROY:
        Quit(0);
        break;
    case WM_SIZE:
        SendMessage(hToolbar, TB_AUTOSIZE, 0, 0);
        break;
    case WM_COMMAND:
        handleMenuEvents(hWnd, wParam);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
        break;
    }

    return 0;
}

void Quit(int exitCode)
{
    PostQuitMessage(exitCode);
}

void handleMenuEvents(HWND hWnd, WPARAM wParam)
{
    switch (LOWORD(wParam))
    {
    case UI::Menu::FILE_EXIT:
        Quit(0);
        break;
    case UI::Menu::ABOUT_HELP:
        MessageBox(hWnd, _T("Help"), _T("About"), MB_OK);
        break;
    case UI::Menu::TOOLBAR_BTN_HOME:
        MessageBox(hWnd, _T("E.T. GO HOME"), _T("Toolbar"), MB_OK);
        break;
    case UI::Menu::TOOLBAR_BTN_BACK:
        MessageBox(hWnd, _T("Going back!"), _T("Toolbar"), MB_OK);
        break;
    case UI::Menu::TOOLBAR_BTN_FORWARD:
        MessageBox(hWnd, _T("Going forward!"), _T("Toolbar"), MB_OK);
        break;
    case UI::Menu::TOOLBAR_BTN_GO:
        MessageBox(hWnd, _T("Going to URL!"), _T("Toolbar"), MB_OK);
    }
}