#include <Windows.h>
#include <string.h>
#include <tchar.h>
#include <CommCtrl.h>
#include "UI.h"
#include "ZaxBrowser.h"

HWND UI::createWindow(HINSTANCE hInstance)
{
    HWND hWnd = CreateWindowEx(
        WS_EX_OVERLAPPEDWINDOW,             // Window Style
        szWindowClass,                      // Window Class (AppName)
        szTitle,                            // Window Title 
        WS_OVERLAPPEDWINDOW | WS_MAXIMIZE,  // Window Type
        CW_USEDEFAULT, CW_USEDEFAULT,       // Window Initial Position
        1024, 768,                          // Window Size (width, height)
        NULL,                               // Parent of window
        NULL,                               // Menu Bar
        hInstance,                          // first WinMain Param (instance)
        NULL                                // not used in this app
    );

    return hWnd;
}

WNDCLASSEX UI::prepareWindow(HINSTANCE hInstance)
{
    WNDCLASSEX wcex{};

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(wcex.hInstance, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

    return wcex;
}

int UI::paintWindow(HWND hWnd, HINSTANCE hInstGlobal, HMENU hMenu, HWND hToolbar)
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint(hWnd, &ps);
    TCHAR greeting[] = _T("Hello world");


    // Display Buttons
   // UI::paintButtons(hWnd, hInstGlobal);

    // Print Toolbar
    UI::paintToolbar(hWnd, hInstGlobal, hToolbar);

    // Print sentence
    //TextOut(hdc,
    //    5, 50,
    //    greeting, _tcslen(greeting));
    // End application-specific layout section.

    EndPaint(hWnd, &ps);
    return 0;
}

int UI::paintButtons(HWND hWnd, HINSTANCE hInstGlobal)
{
    // Home button
    HWND btnHome = CreateWindowEx(NULL,
        L"BUTTON",
        L"hee loooo",
        WS_TABSTOP | WS_VISIBLE |
        WS_CHILD | BS_DEFPUSHBUTTON | BS_ICON | BS_CENTER,
        0, 0,
        25, 25,
        hWnd,
        NULL,
        hInstGlobal,
        NULL
    );

    // Set Icon on Home button
    HANDLE hImg = LoadImageW(NULL, L"Data/Images/Home.ico", IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR | LR_DEFAULTSIZE | LR_LOADTRANSPARENT | LR_LOADFROMFILE);
    SendMessageW(btnHome, BM_SETIMAGE, IMAGE_ICON, (LPARAM)hImg);

    return 0;
}

int UI::paintToolbar(HWND hWnd, HINSTANCE hInstGlobal, HWND hToolbar)
{
    const int toolBarItemCount = 4;
    const int btnSize = 22;

    hToolbar = CreateWindowEx(WS_EX_TOOLWINDOW, 
        TOOLBARCLASSNAME, 
        NULL,
        WS_CHILD | TBSTYLE_LIST,
        0, 3,
        0, 200,
        hWnd,
        NULL,
        hInstGlobal,
        NULL
    );

    if (!hToolbar)
    {
        MessageBox(NULL, _T("Tool Bar Failed."), _T("Error"), MB_OK | MB_ICONERROR);
        return -1;
    }

    SendMessage(hToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
    SendMessage(hToolbar, TB_SETBITMAPSIZE, (WPARAM)0, (LPARAM)MAKELONG(25, 25));
    SendMessage(hToolbar, TB_SETEXTENDEDSTYLE, (WPARAM)0, (LPARAM) 0);
// SendMessage(hToolbar, TB_ADDBITMAP, (WPARAM)2, (LPARAM)&tbab);

    HWND hBtnBack = CreateWindowEx(
        0L,
        _T("Button"),
        NULL,
        WS_VISIBLE | WS_CHILD | BS_ICON | BS_CENTER,
        2, 0,
        btnSize, btnSize,
        hToolbar,
        (HMENU)UI::Menu::TOOLBAR_BTN_BACK,
        hInstGlobal,
        0
    );

    HWND hBtnForward = CreateWindowEx(
        0L,
        _T("Button"),
        NULL,
        WS_VISIBLE | WS_CHILD | BS_ICON | BS_CENTER,
        25, 0,
        btnSize, btnSize,
        hToolbar,
        (HMENU)UI::Menu::TOOLBAR_BTN_FORWARD,
        hInstGlobal,
        0
    );

    HWND hBtnHome = CreateWindowEx(
        0L,
        _T("Button"),
        NULL,
        WS_VISIBLE | WS_CHILD | BS_ICON | BS_CENTER,
        57, 0,
        btnSize, btnSize,
        hToolbar,
        (HMENU)UI::Menu::TOOLBAR_BTN_HOME,
        hInstGlobal,
        0
    );
    HANDLE hImgHome = LoadImageW(NULL, L"Data/Images/Home.ico", IMAGE_ICON, btnSize - 2, btnSize - 2, LR_DEFAULTCOLOR  | LR_LOADTRANSPARENT | LR_LOADFROMFILE);
    SendMessageW(hBtnHome, BM_SETIMAGE, IMAGE_ICON, (LPARAM)hImgHome);
        
    // URL bar
    HWND hEditUrl = CreateWindowEx(
        0L, 
        _T("Edit"), 
        NULL,
        WS_CHILD | WS_BORDER | WS_VISIBLE | ES_LEFT | ES_AUTOVSCROLL | ES_MULTILINE,
        80, 0, 
        500, 22,
        hToolbar, 
        NULL,
        hInstGlobal, 
        0
    );

    HWND hBtnGo = CreateWindowEx(
        0L,
        _T("Button"),
        NULL,
        WS_VISIBLE | WS_CHILD | BS_ICON | BS_CENTER,
        581, 0,
        btnSize, btnSize,
        hToolbar,
        (HMENU) UI::Menu::TOOLBAR_BTN_GO,
        hInstGlobal,
        0
    );

    ShowWindow(hToolbar, SW_SHOW);
    return 0;
}

HMENU UI::createMenu(HWND hWnd)
{
    HMENU hMenu, hSubMenu;

    hMenu = CreateMenu();

    // File
    hSubMenu = CreatePopupMenu();
    AppendMenu(hSubMenu, MF_STRING, UI::Menu::FILE_EXIT, _T("E&xit"));
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("&File"));

    // Edit
    hSubMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("&Edit"));

    // View
    hSubMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("&View"));

    // History
    hSubMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("&History"));

    // Bookmarks
    hSubMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("&Bookmarks"));

    // About
    hSubMenu = CreatePopupMenu();
    AppendMenu(hSubMenu, MF_STRING | MF_DISABLED, UI::Menu::ABOUT_HELP, _T("&Help"));
    AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, _T("&About"));

    SetMenu(hWnd, hMenu);
    return hMenu;

   /* hIcon = LoadImage(NULL, "menu_two.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
    // HICON hIcon, hIconSm;
    if (hIcon)
        SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
    else
        MessageBox(hWnd, "Could not load large icon!", "Error", MB_OK | MB_ICONERROR);

    hIconSm = LoadImage(NULL, "menu_two.ico", IMAGE_ICON, 16, 16, LR_LOADFROMFILE);
    if (hIconSm)
        SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)hIconSm);
    else
        MessageBox(hWnd, "Could not load small icon!", "Error", MB_OK | MB_ICONERROR);*/
}