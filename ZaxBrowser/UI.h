#pragma once

namespace UI {
	HWND createWindow(HINSTANCE hInstance);
	HMENU createMenu(HWND hWnd);
	WNDCLASSEX prepareWindow(HINSTANCE hInstance);
	int paintWindow(HWND hWnd, HINSTANCE hInstGlobal, HMENU hMenu, HWND hToolbar);
	int paintButtons(HWND hWnd, HINSTANCE hInstGlobal);
	int paintToolbar(HWND hWnd, HINSTANCE hInstGlobal, HWND hToolbar);

	enum Menu {
		MENU_MAIN = 100,
		FILE_EXIT,
		ABOUT_HELP,


		TOOLBAR_BTN_BACK,
		TOOLBAR_BTN_FORWARD,
		TOOLBAR_BTN_HOME,
		TOOLBAR_BTN_GO,
	};
}