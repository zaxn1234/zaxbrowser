#pragma once
#include <Windows.h>
#include <tchar.h>

// Global variables

// The main window class name.
static TCHAR szWindowClass[] = _T("DesktopApp");

// The string that appears in the application's title bar.
static TCHAR szTitle[] = _T("ZaxBrowser (Atlas)");

// Forward declarations of functions included in this code module:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void handleMenuEvents(HWND hWnd, WPARAM wParam);
void Quit(int exitCode);

